<div align="center">
    <h1>instawm</h1>
    <p>Window manager for linux</p>
</div>

![instawm the window manager for linux.](https://raw.githubusercontent.com/The-Repo-Club/instawm/main/screenshot.png)

## Installation

**For Arch** it can be founded in [AUR](https://aur.archlinux.org/packages/instawm) \
**For Gentoo** refer to the [XDream's Repository](https://github.com/XDream8/dreamsrepo) \
**Recommended Method**
```sh
$ git clone https://github.com/The-Repo-Club/instawm.git
$ cd instawm
# make install
```

[Download latest release](https://github.com/The-Repo-Club/instawm/releases/)

## Features

This is just a quick list of some features. For a full list and explanation,
please refer to the documentation.

- General
  * hybrid-wm: tiling and floating mode
  * Keyboard and Mouse based workflows
  * start menu
  * full multi monitor support
  * tag system
  * instabar script
  * indicators
  * shutdown menu
  * system tray
  * window gaps
  * restart function


## is this dwm?

instamenu is a fork of [DWM](https://dwm.suckless.org) and can be used as a drop in replacement, maintaining all dmenu behavior and making all extra features optional some features are also borrowed from [InstantWM](https://github.com/instantOS/instantWM)
